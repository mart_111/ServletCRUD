<%--
  Created by IntelliJ IDEA.
  User: martinknyazyan
  Date: 2019-02-21
  Time: 15:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign Up</title>
</head>

<style>

    input {
        margin: 0 auto;
    }

</style>

<body>

<form action="/sign-up" method="post">
    <label for="name">Name:</label><input type="text" name="name" id="name"><br><br>
    <label for="surname">Surname:</label><input type="text" name="surname" id="surname"><br><br>
    <label for="username">Username:</label><input type="text" name="username" id="username"><br><br>
    <label for="password">Password:</label><input type="password" name="password" id="password"><br><br>
    <input type="submit" value="Add User" style="font-size: 19px">

</form>

</body>
</html>
