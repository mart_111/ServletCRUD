<%--
  Created by IntelliJ IDEA.
  User: martinknyazyan
  Date: 2019-02-21
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title>Title</title>
</head>

<style>

    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

</style>

<body>


<table border="1">
    <!-- here should go some titles... -->
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Username</th>
        <th>Password</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                <c:out value="${user.name}"/>
            </td>
            <td>
                <c:out value="${user.surname}"/>
            </td>
            <td>
                <c:out value="${user.username}"/>
            </td>
            <td>
                <c:out value="${user.password}"/>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
