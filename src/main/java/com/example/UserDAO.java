package com.example;

import java.util.ArrayList;
import java.util.List;

/**
 * DAO (Data Access Object) pattern based class to do simple CRUD
 * operations for user.
 */

public class UserDAO {

    public static List<User> users = new ArrayList<>();

    public static void save(User user) {
        if (!users.isEmpty()) {
            int id = users.get(users.size() - 1).getId() + 1;
            user.setId(id);
            users.add(user);
        } else users.add(user);
    }

    public static void delete(User user) {
        if (!users.isEmpty()) {
            users.remove(user);
        } else throw new RuntimeException("User List is empty");
    }

    public static void update(User user) {

        if (!users.isEmpty()) {

            for (User u : users) {
                if (u.equals(user)) {
                    int index = users.indexOf(u);
                    users.set(index, user);
                }
            }
        } else throw new RuntimeException("User List is empty");
    }

    public static User getUserById(int id) {
        if (!users.isEmpty()) {
            return users.stream()
                    .filter(user -> user.getId() == id)
                    .findFirst()
                    .get();
        } else throw new RuntimeException("User List is empty");
    }

    public static List<User> getAllUsers() {
        return !users.isEmpty() ? users : new ArrayList<>();
    }

}
